import { formatInput } from '../src/utils';
import { EncodeDecode } from '../src/encodedecode';
import { testData } from './testData';

describe('Test EncodeDecode', function () {
    let ed;

    beforeAll(function () {
        ed = new EncodeDecode();
    });

    testData.forEach(function (data) {
        // Trying to encode string that contains characters that are not included in the alphabet
        // Encode function won't break if input string has symbols outside of alphabet scope, it will
        // just skip the characters.
        it(`encode string: ${data.input} - should fail`, function () {
            let result = ed.encode(data.input);
            expect(result).not.toBe(data.encoded);
        });

        // Test encode function
        // We have to always format input value before sending it to encode function
        // As an alternative implementation formatInput() could be called from inside the encode function.
        it(`encode string: ${data.input} - should pass`, function () {
            // Format input before passing it to the encode function
            let formatted = formatInput(data.input);

            if (formatted) {
                let result = ed.encode(formatted);

                expect(formatted).toBe(data.formatted);
                expect(result).toBe(data.encoded);
            } else {
                expect(formatted).toBeNull();
            }
        });

        // Test decode function, due to input data format, encoded output may not exist
        if (data.encoded) {
            it(`decode string: ${data.encoded} - should pass`, function () {
                let result = ed.decode(data.encoded);
                expect(result).toBe(data.formatted);
            });
        }
    })
});