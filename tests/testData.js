export const testData = [
    {
        input: 'ENCODE',
        formatted: 'encode',
        encoded: 'ksgrff'
    },
    {
        input: 'e n c o d e',
        formatted: 'encode',
        encoded: 'ksgrff'
    },
    {
        input: 'fuzzy123',
        formatted: null,
        encoded: null
    },
    {
        input: 'z z z z ZZZ zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
        formatted: 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
        encoded: 'vutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba'
    }
];