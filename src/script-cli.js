import { formatInput } from './utils';
import { EncodeDecode } from './encodedecode';

const ed = new EncodeDecode();

// Get process.stdin as the standard input object.
const standard_input = process.stdin;

// Set input character encoding.
standard_input.setEncoding('utf-8');

const promptMessage = 'Please enter a string that you want to Encode or Decode:';

console.log(promptMessage);

// When user input data and click enter key.
standard_input.on('data', function (data) {
	const input = formatInput(data);

	if (input) {
		console.log(`Encoded string: ${ed.encode(input)}`);
		console.log(`Decoded string:  ${ed.decode(input)}`);
		console.log(promptMessage);
	} else {
		console.log('Please enter string that contains letters only.');
	}
});