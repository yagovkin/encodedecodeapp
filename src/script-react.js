import { formatInput, isValidString } from './utils.js'
import { EncodeDecode } from './encodedecode.js'

'use strict';

const el = React.createElement;

class EncodeDecodeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            encode: {
                input: '',
                output: ''
            },
            decode: {
                input: '',
                output: ''
            }
        };
        this.ed = new EncodeDecode();
    };

    isValid = (value) => isValidString(value);

    getValidationClass = (value) => {
        if (value === '') {
            return '';
        } else {
            return this.isValid(value) ? 'is-valid' : 'is-invalid';
        }
    };

    onInputChange = (e, option) => {
        option === 'encode' ?
            this.setState({
                encode: {
                    input: e.target.value,
                    output: ''
                }
            }) :
            this.setState({
                decode: {
                    input: e.target.value,
                    output: ''
                }
            });
    };

    encodeString = (value) => {
        const formatted = formatInput(value);

        this.setState({
            encode: {
                input: value,
                output: this.ed.encode(formatted)
            }
        });
    };

    decodeString = (value) => {
        this.setState({
            decode: {
                input: value,
                output: this.ed.decode(value)
            }
        });
    };

    render() {
        return el('div', { className: 'card-deck' },

            el('div', { className: 'card col-sm-6' },
                el('div', { className: 'card-body' },
                    el('h4', { className: 'card-title' }, 'Encode string:'),
                    el('input', {
                        type: 'text',
                        className: `form-control ${(this.getValidationClass(this.state.encode.input))}`,
                        placeholder: 'Value to encode',
                        value: this.state.encode.input,
                        onChange: (e) => {
                            this.onInputChange(e, 'encode')
                        }
                    }),
                    el('div', { className: 'invalid-feedback' }, 'Please enter a string that contains letters only!'),
                    el('h6', { className: 'mt-3 mb-3' }, 'Encoded string:'),
                    el('div', { className: this.state.encode.output ? 'alert alert-success' : '' }, this.state.encode.output),
                    el('button', {
                        className: 'btn btn-primary float-right',
                        disabled: !this.isValid(this.state.encode.input),
                        onClick: () => this.encodeString(this.state.encode.input)
                    }, 'Encode')
                )
            ),
            el('div', { className: 'card col-sm-6' },
                el('div', { className: 'card-body' },
                    el('h4', { className: 'card-title' }, 'Decode string:'),

                    el('div', { className: 'input-group mb-3' },
                        el('input', {
                            type: 'text',
                            className: `form-control ${(this.getValidationClass(this.state.decode.input))}`,
                            placeholder: 'Value to decode',
                            value: this.state.decode.input,
                            onChange: (e) => {
                                this.onInputChange(e, 'decode')
                            }
                        }),
                        el('div', { className: 'input-group-append' },
                            el('button', {
                                className: 'btn btn-outline-secondary',
                                disabled: !this.isValid(this.state.encode.output),
                                onClick: () => { this.setState({ decode: { input: this.state.encode.output, output: '' } }) }
                            }, 'Copy encoded')
                        ),
                        el('div', { className: 'invalid-feedback' }, 'Please enter a string that contains letters only!'),
                    ),
                    el('h6', { className: 'mt-3 mb-3' }, 'Decoded string:'),
                    el('div', { className: this.state.decode.output ? 'alert alert-success' : '' }, this.state.decode.output),
                    el('button', {
                        className: 'btn btn-primary float-right',
                        disabled: !this.isValid(this.state.decode.input),
                        onClick: () => this.decodeString(this.state.decode.input)
                    }, 'Decode')
                )
            )
        );
    };
};

ReactDOM.render(
    el(EncodeDecodeForm),
    document.getElementById('encode_button_container')
);