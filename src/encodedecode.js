export class EncodeDecode {
    constructor() {
        this.alphabet = this.buildAlphabetObject();
    };

    buildAlphabetObject() {
        // Alphabet range. Could easily be extended by adding Capital letters and numbers
        const alphabetStr = 'abcdefghijklmnopqrstuvwxyz';
        const alphabet = {};

        alphabetStr.split('').forEach(function (letter, i) {
            alphabet[letter] = i + 1;
        });

        return alphabet;
    }

    getLetterByKey(letterKey) {
        const alphabetLength = Object.keys(this.alphabet).length;

        // Handle case when letter index smaler of greater than indexes of symbols in alphabet
        if (letterKey < 1 || letterKey > alphabetLength) {
            letterKey = letterKey % alphabetLength;

            if (letterKey < 0) {
                letterKey = letterKey + alphabetLength;
            }

            if (letterKey === 0) {
                letterKey = alphabetLength;
            }
        }

        return Object.keys(this.alphabet).find(key => this.alphabet[key] === letterKey);
    }

    processInput(input, operation) {
        const result = [];
        const getLetterByKey = (k) => this.getLetterByKey(k);
        const alphabet = this.alphabet;

        input.split('').reverse().forEach(function (letter, i) {
            const letterKey = operation === 'encode' ? alphabet[letter] + i + 1 : alphabet[letter] - i - 1;
            result.push(getLetterByKey(letterKey));
        });

        return result.reverse().join('');
    }


    encode(input) {
        return this.processInput(input, 'encode');
    };

    decode(input) {
        return this.processInput(input, 'decode');
    };
}