// Make input sutable for encoding: remove spaces, tabs, new lines, convert to lowercase...
export function formatInput(value) {
	let processedStr = value.replace(/(\s|\r\n|\n|\r)/g, '').trim();

	if (processedStr.length > 0) {
		const letters = /^[a-z]+$/;

		processedStr = processedStr.toLowerCase();

		return processedStr.match(letters) ? processedStr : null;
	}

	return null;
};

export function isValidString(value) {
	return /^[a-zA-Z]+$/.test(value);
};