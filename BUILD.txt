System requirements: NodeJS version 8+

Install dependencies:
npm install

Run web app:
- Place the project on web server and access index.html in browser
- If using VS Code, Live Server extension could be used: https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer

Run console app:
npm start

Run unit tests:
npm test