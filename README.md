#Digital Frontend Application Developer Coding Challenge

##Develop an application with two operations:
* encode
* decode
The encode operation should:
* Accept a word 
* For each letter in the word: 
   * Determine the position of the letter relative to the end of the word e.g. in 'Encode', 'E' is in position 6, 'n' is in position 5, etc.
   * Shift the letter forward by x letters, where x is the position of the letter determined above. e.g. in 'Encode', 'E' becomes 'K', 'n' becomes 's', etc.  If you get to Z/z, start at A/a.
* Return the result
For example:

The decode operation should:
* Accept a word that has been encoded as per the above
* Decode it
* Return the result
Develop the app using one or more design patterns. We want to see how you design your application and structure your code, not necessarily whether you finish the challenge.
Please we prefer the use of either React, Angular, Vue main framework, but we ask you refrain from using Typescript in your implementation (stick to standard JavaScript).
The solution must contain an interface to handle inputs and output. This is not a test of your UI design skills, but it must be usable on both a desktop and mobile device.  
Your project should include:
* a DESIGN.txt file that documents any design decisions you have made
* a BUILD.txt file that explains how to build and run your application
Zip up your project and return it via email
